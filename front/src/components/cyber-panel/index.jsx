const CyberPanel = ({ legend, content, isSelected }) => {
    if (legend) {
        return (<div><h1 className={isSelected ? "cyber-panel-legend cyber-panel-legend-selected" : "cyber-panel-legend"}>{legend}</h1><div className={isSelected ? "cyber-panel cyber-panel-selected" : "cyber-panel"}>{content}</div></div>)
    }
    return (<div className={isSelected ? "cyber-panel cyber-panel-selected" : "cyber-panel"}>{content}</div>)
}

export default CyberPanel