import React from 'react'
import jobsCreate from '../components/content/jobs-create/index'
import { fireEvent, render } from '@testing-library/react'

const mockSetApi = jest.fn()

beforeEach(() => {
    jest.clearAllMocks()
})

describe('app Component', () => {
  it('when trying to introduce Mikoshi', () => {
    const appRender = render(
      <button aria-label='access-mikoshi'>ACC.E?S T0 MI?K.OSHI</button>
    );

    const mikoshiButton = appRender.getByRole('button', { name: 'access-mikoshi' })
    mikoshiButton.click();
    expect();
  })
});

