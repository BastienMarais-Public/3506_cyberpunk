import axios from 'axios';
import { 
    createJob,
    getJobs,
    createWeapon,
    getWeapons,
    createMerc,
    getMercs,
    launchMission,
    buyWeapon
} from '../api/callApi'

const mockSetInfo =  jest.fn()
const mockLaunchRefresh = jest.fn()
const mockSetJobs =  jest.fn()
const mockSetWeapons =  jest.fn()
const mockSetMercs =  jest.fn()
const mockSetReport =  jest.fn()

jest.mock('axios');

beforeEach(() => {
    jest.clearAllMocks()
})

describe('Test call api', () => {
    it('When creating a job', () => {
        
        const job = {
            fixer: "Judy",
            title: "Pyramid song",
            description: "Hey V, can you come to the city border please ? I wanna go scuba diving with you",
            henchmenCount: 0,
            reward: 69, //😏
        }
        
        const response = {
            status: 200,
            data: job
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        createJob(job, mockSetInfo, mockLaunchRefresh)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
    });

    it('When getting jobs', () => {
        
        const job = "Venus in Furs"
        const response = {
            status: 200,
            data: job
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        createJob(job, mockSetInfo, mockSetJobs)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
    });
    
    it('When creating a weapon', () => {
        
        const weapon = {
            name: "Sire Vergeraide",
            description: "Utilisez-le conformément aux instructions. N'oubliez pas de demander le consentement de la cible. Parfait quand vous ressentez le besoin de tout nicker.",
            damage: 69,
            accuracy: 75,
            firerate: 2,
            price: 80,
        }
        const response = {
            status: 200,
            data: weapon
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        createJob(weapon, mockSetInfo, mockSetWeapons)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
    });

    it('When getting weapons', () => {

        const weapon = "Sire Vergeraide"
        const response = {
            status: 200,
            data: weapon
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        createJob(weapon, mockSetInfo, mockSetWeapons)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockSetWeapons).toHaveBeenCalledTimes(0)
    });

    it('When creating a merc', () => {
        
        const merc = {
            nickname: "Jackie Welles",
            legalAge: 30,
            eddies: 3700,
        }
        const response = {
            status: 200,
            data: merc,
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        createJob(merc, mockSetInfo, mockSetMercs)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
    })

    it('When getting mercs', () => {
        
        const merc = "Jackie Welles"
        const response = {
            status: 200,
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        createJob(merc, mockSetInfo, mockSetMercs)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
    });

    it('when launching the mission', () => {

        const mercId = 1;
        const jobId = 1;

        const response = {
            status: 200,
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        launchMission(jobId, mercId, mockSetInfo, mockSetReport, mockLaunchRefresh)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockSetReport).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
    });

    it('When buying weapon', () => {
        
        const mercId = 1
        const weaponId = 1

        axios.get.mockImplementationOnce(() => Promise.resolve(response));
        createMerc(mercId, weaponId, mockSetInfo, mockLaunchRefresh)
        expect(mockSetInfo).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
        expect(mockLaunchRefresh).toHaveBeenCalledTimes(0)
    });
})