import express from 'express'
import bodyParser from 'body-parser'
import { getNightCity } from '../business/nightCity'
import { NoMercException, NoWeaponException, NoJobException } from '../business/error'
 
const nightCity = getNightCity()
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));


app.use(function (_req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

/**
 * Function that formats api logs
 * @param {string} status OK / KOT / KOF
 * @param {string} action endpoint api
 * @param {string} msg message to be logged 
 */
const log = (status, action, msg) => {
  console.log(`[${status}] ; ${action} ; ${msg}`);
}
/*********************************************************
 * GET
 ********************************************************/

/**
 * Retrive a list of mercs from the database
 * URL: /mercs/:name
 * Parameters:  -	name: the name of the merc we want to find (string)
 */
app.get('/mercs/:name?', async (req, res) => {
  const { name } = req.params;
  try {
    res.status(200).json(await nightCity.getMercsByName(name));
    log("OK", `/mercs/${name}`, "Retrieving the list of Mercs");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/mercs/${name}`, err);
    }
    res.status(500).end();
    log("KOT", `/mercs/${name}`, err);
  }
})

/**
 * Retrive a list of weapons from the database
 * URL: /weapons/:name
 * Parameters:  -	name: the name of the weapon we want to find (string)
 */
app.get('/weapons/:name?', async (req, res) => {
  const { name } = req.params;
  try {
    res.status(200).json(await nightCity.getWeaponsByName(name));
    log("OK", `/weapons/${name}`, "Retrieving the list of Weapons");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/weapons/${name}`, err);
    }
    res.status(500).end();
    log("KOT", `/weapons/${name}`, err);
  }
})

/**
 * Retrive a weapon from its id
 * URL: /weapons/:id
 * Parameters:  -	id: the id of the weapon we want to find (string)
 */
app.get('/weapons/:name?', async (req, res) => {
  const { name } = req.params;
  try {
    res.status(200).json(await nightCity.getWeaponsByName(name));
    log("OK", `/weapons/${name}`, "Retrieving the list of Weapons");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/weapons/${name}`, err);
    }
    res.status(500).end();
    log("KOT", `/weapons/${name}`, err);
  }
})

/**
 * Retrive a list of jobs from the database
 * URL: /job/:title
 * Parameters:  -	title: the title of the job we want to find (string)
 */
app.get('/jobs/:title?', async (req, res) => {
  const { title } = req.params;
  try {
    res.status(200).json(await nightCity.getJobsByTitle(title));
    log("OK", `/jobs/${title}`, "Retrieving the list of jobs");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/jobs/${title}`, err);
    }
    res.status(500).end();
    log("KOT", `/jobs/${title}`, err);
  }
})

/*********************************************************
 * POST
 ********************************************************/

/**
 * Declares a new Merc in the database
 * URL: /merc/create
 * Parameters:  -	name: the name of the merc (string)
 *              -	legalAge: age of the merc (int, positive)
 */
app.post('/merc/create', async (req, res) => {
  let { nickname, legalAge, eddies } = req.body;
  legalAge = legalAge !== "" ? Number(legalAge) : null
  eddies = eddies !== "" ? Number(eddies) : null

  try {
    const merc = await nightCity.createMerc(nickname, legalAge, eddies);
    res.status(200).json(merc);
    log("OK", `/merc/create`, "Merc created");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/merc/create`, err);
    }
    res.status(500).end();
    log("KOT", `/merc/create`, err);
  }
})

/**
 * Declares a new Weapon in the database
 * URL: /weapon/create
 * Parameters:  -	name: the name of the weapon (string)
 *              -	description: the description of the weapon (string)
 *              - damage: accuracy and firerate: weapon stats (int)
 *              - price: the price of the weapon (int)
 */
app.post('/weapon/create', async (req, res) => {
  let { name, description, damage, accuracy, firerate, price } = req.body;
  damage = damage !== "" ? Number(damage) : null
  accuracy = accuracy !== "" ? Number(accuracy) : null
  firerate = firerate !== "" ? Number(firerate) : null
  price = price !== "" ? Number(price) : null

  try {
    const weapon = await nightCity.createWeapon(name, description, damage, accuracy, firerate, price);
    res.status(200).json(weapon);
    log("OK", `/weapon/create`, "Weapon created");
  } catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/weapon/create`, err);
    }
    nightCity.testException(err);
    res.status(500).end();
    log("KOT", `/weapon/create`, err);
  }
})

/**
 * Declares a new Job in the database
 * URL: /job/create
 * Parameters:  -	fixer: the name of the fixer (string)
 *              -	title: the title of the job (string)
 *              -	description: the description of the job (string)
 *              - henchmenCount: the number of foes (int)
 *              - reward: the reward value (int)
 */
app.post('/job/create', async (req, res) => {
  let { fixer, title, description, henchmenCount, reward } = req.body;
  henchmenCount = henchmenCount !== "" ? Number(henchmenCount) : null
  reward = reward !== "" ? Number(reward) : null
  try {
    const job = await nightCity.createJob(fixer, title, description, henchmenCount, reward);
    res.status(200).json(job);
    log("OK", `/job/create`, "Job created");
  } catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/job/create`, err);
    }
    res.status(500).end();
    log("KOT", `/job/create`, err);
  }
})

/**
 * Launch the mission !
 * URL: /job/start
 * Parameters:  -	job_id: the job ID (int)
 *              -	merc_id: the merk ID (int)
 */
app.post('/job/start/:job_id/:merc_id', async (req, res) => {
  const { job_id, merc_id } = req.params;
  try {
    const report = await nightCity.executeMission(Number(job_id), Number(merc_id));
    res.status(200).json(report);
    log("OK", `/job/start/${job_id}/${merc_id}`, "Job started");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/weapons`, err);
    }
    res.status(500).end();
    log("KOT", `/job/start/${job_id}/${merc_id}`, err);
  }
})

/**
 * Buy and assign a new Weapon to a merc
 * URL: /weapon/buy/:merc_id/:weapon_id
 * Parameters:  -	merc_id: the merk ID (int)
 *              -	weapon_id: the weapon ID (int)
 */
app.post('/weapon/buy/:merc_id/:weapon_id', async (req, res) => {
  const { merc_id, weapon_id } = req.params;
  try {
    const merc = await nightCity.buyWeapon(Number(merc_id), Number(weapon_id));
    res.status(200).json(merc);
    log("OK", `/weapon/buy/${merc_id}/${weapon_id}`, "Weapon updated");
  } catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/weapons`, err);
    }
    res.status(500).end();
    log("KOT", `/weapon/buy/${merc_id}/${weapon_id}`, err);
  }
})

// /*********************************************************
// * PUT
// ********************************************************/

// UPDATE MERC / JOB / WEAPON  avec l'objet en query /!\

/**
 * update an existing Weapon in the database
 * URL: /weapon/:id
 * Parameters:  -	name: the name of the weapon (string)
 *              -	description: the description of the weapon (string)
 *              - damage, accuracy and firerate: weapon stats
 *              - price: the price of the weapon
 */
app.put('/weapon/:id', async (req, res) => {
  const { name, description, damage, accuracy, firerate, price } = req.body;
  const id = req.params.id;
  try {
    const weapon = await nightCity.updateWeapon(Number(id), name, description, Number(damage), Number(accuracy), Number(firerate), Number(price));
    res.status(200).json(weapon);
    log("OK", `/weapon/${id}`, "Weapon updated");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/weapons/${id}`, err);
    }
    res.status(500).end();
    log("KOT", `/weapon/${id}`, err);
  }
})

/**
 * update an existing Merc in the database
 * URL: /merc/:id
 * Parameters:  -	nickname: the nickname of the merc (string)
 *              -	legalAge: the age of the merc (int)
 *              - idWeapon: the marc's weapon id (int)
 *              - eddies: the merc eddies (int)
 *              - isAlive: the merc status (boolean)
 */

app.put('/merc/:id', async (req, res) => {
  const { nickname, legalAge, idWeapon, eddies, isAlive } = req.body;
  const id = req.params.id;
  try {
    const merc = await nightCity.updateMerc(Number(id), nickname, Number(legalAge), Number(idWeapon), Number(eddies), Boolean(isAlive));
    res.status(200).json(merc);
    log("OK", `/merc/${id}`, "Merc updated");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/merc/${id}`, err);
    }
    res.status(500).end();
    log("KOT", `/merc/${id}`, err);
  }
})

/**
 * update an existing Job in the database
 * URL: /job/:id
 * Parameters:  -	fixer: fixer name (string)
 *              -	title: job title (string)
 *              - description: job description (string)
 *              - henchmenCount: the henchmen number (int)
 *              - reward: the reward amount (int)
 *              - isAvailable: the job status (boolean)
 */

app.put('/job/:id', async (req, res) => {
  const { fixer, title, description, henchmenCount, reward, isAvailable } = req.body;
  const id = req.params.id;
  try {
    const job = await nightCity.updateJob(Number(id), fixer, title, description, Number(henchmenCount), Number(reward), Boolean(isAvailable));
    res.status(200).json(job);
    log("OK", `/job/${id}`, "Job updated");
  }
  catch (err) {
    if (nightCity.testException(err)) {
      res.status(400).json(err.message);
      log("KOF", `/job/${id}`, err);
    }
    res.status(500).end();
    log("KOT", `/job/${id}`, err);
  }
})

export default app;