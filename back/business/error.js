
export class NoMercException extends Error {
    constructor(){
        super("No merc with a such name")
    }
}

export class NoWeaponException extends Error {
    constructor(){
        super("No weapon with such name")
    }
}
export class NoJobException extends Error {
    constructor(){
        super("No job with such title")
    }
}
export class UnexistingMercException extends Error {
    constructor(){
        super("No merc with such ID")
    }
}
export class UnexistingJobException extends Error {
    constructor(){
        super("No job with such ID")
    }
}
export class UnexistingWeaponException extends Error {
    constructor(){
        super("No weapon with such ID")
    }
}

export class NoIdException extends Error {
    constructor(){
        super("No IDs retrieved")
    }
}

export class NotEnoughEddiesException extends Error {
    constructor(){
        super("This merc doesn't have enough eddies to buy this weapon")
    }
}

export class DeadMercException extends Error {
    constructor(){
        super("This merc is already dead, remember ?")
    }
}

export class CompletedJobException extends Error {
    constructor(){
        super("This mission has already been completed")
    }
}

export class NoHenchmenException extends Error {
    constructor(){
        super("You need to properly indicate the henchmen number")
    }
}

export class NoFixerException extends Error {
    constructor(){
        super("You need to refer a fixer for this job")
    }
}

export class NoTitleException extends Error {
    constructor(){
        super("You need set a title for this job")
    }
}

export class NoRewardException extends Error {
    constructor(){
        super("You need to give at least 1 eddie for the reward, you greedy lad")
    }
}

export class NoNicknameException extends Error {
    constructor(){
        super("You forgot to indicate the merc's nickname")
    }
}

export class NoLegalAgeException extends Error {
    constructor(){
        super("You forgot to indicate the merc's legalAge")
    }
}

export class NoEddiesException extends Error {
    constructor(){
        super("You forgot to indicate the eddies")
    }
}
export class NoNameException extends Error {
    constructor(){
        super("You can't create a weapon with this name")
    }
}

export class NoDescriptionException extends Error {
    constructor(){
        super("You need to refer a description")
    }
}

export class NoDamageException extends Error {
    constructor(){
        super("You can't create a weapon with this damage")
    }
}

export class NoAccuracyException extends Error {
    constructor(){
        super("You can't create a weapon with this accuracy")
    }
}

export class NoFirerateException extends Error {
    constructor(){
        super("You can't create a weapon with this firerate")
    }
}

export class NoPriceException extends Error {
    constructor(){
        super("You can't create a weapon with this price")
    }
}
