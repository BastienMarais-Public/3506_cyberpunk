
import { Like } from "typeorm"
import Database from '../model/database'
import Mercs from '../model/entity/mercs'
import Jobs from '../model/entity/jobs'
import Weapons from '../model/entity/weapons'
import Report from '../model/entity/report'
import ReportLine from '../model/entity/reportLine'
import * as exc from './error'

class NightCity {

  /**
   * Creates an instance of NightCity.
   * @memberof NightCity
   */
  constructor() {
    this.db = new Database();
  }

  testException(err) {
    //console.log(err)
    if(
      err instanceof exc.NoMercException ||
      err instanceof exc.NoWeaponException ||
      err instanceof exc.NoJobException ||
      err instanceof exc.UnexistingMercException ||
      err instanceof exc.UnexistingJobException ||
      err instanceof exc.UnexistingWeaponException ||
      err instanceof exc.NoIdException ||
      err instanceof exc.NotEnoughEddiesException ||
      err instanceof exc.DeadMercException ||
      err instanceof exc.CompletedJobException ||
      err instanceof exc.NoHenchmenException ||
      err instanceof exc.NoFixerException ||
      err instanceof exc.NoTitleException ||
      err instanceof exc.NoRewardException ||
      err instanceof exc.NoNicknameException ||
      err instanceof exc.NoLegalAgeException ||
      err instanceof exc.NoEddiesException ||
      err instanceof exc.NoNameException ||
      err instanceof exc.NoDescriptionException ||
      err instanceof exc.NoDamageException ||
      err instanceof exc.NoAccuracyException ||
      err instanceof exc.NoFirerateException ||
      err instanceof exc.NoPriceException
    ) {
      return true;
    }
    return false;
  }

  /*********************************************************
   * WEAPON
   ********************************************************/

  /**
   * Add a weapon to the database
   * @param {String} name weapon name
   * @param {String} description weapon description
   * @param {Number} damage weapon damage
   * @param {Number} accuracy weapon accuracy
   * @param {Number} firerate weapon firerate
   * @param {Number} price weapon price
   * @return {weapons: Weapons} the new weapon
   * @memberof NightCity
   */
  async createWeapon(name, description, damage, accuracy, firerate, price) {
    if(this.checkWeaponInput(name, description, damage, accuracy, firerate, price)){
      try {
        return this.db.addData(Weapons, new Weapons(null, name, description, damage, accuracy, firerate, price));
      }
      catch (err) {
        throw err;
      }
    }
  }

  checkWeaponInput(name, description, damage, accuracy, firerate, price){

    if (!name || name.trim() === "") {
      throw new exc.NoNameException();
    }
    if(!description) {
      throw new exc.NoDescriptionException();
    }
    if((damage !== 0 && !damage) || damage <= 0) {
      throw new exc.NoDamageException();
    }
    if((accuracy !== 0 && !accuracy) || accuracy <= 0) {
      throw new exc.NoAccuracyException();
    }
    if((firerate !== 0 && !firerate) || firerate <= 0) {
      throw new exc.NoFirerateException();
    }
    if((price !== 0 && !price) || price <= 0) {
      throw new exc.NoPriceException();
    }
    else {
      return true;
    }
  }
  
  /**
   * Find a list of weapons using their names
   * @param {String} name weapon name
   */
  async getWeaponsByName(name) {
    try {
      if (name) {
        const weapons = await this.db.getData(Weapons, { where: { name: Like(`%${name}%`) }, order: {id:"DESC"} });
        if (weapons.length == 0)
          throw new exc.NoWeaponException();
        return weapons;
      }
      return await this.db.getData(Weapons, {order: {id:"DESC"} });
    }
    catch (err) {
      throw err;
    }
  }

  /**
   * Update a weapon to the database
   * @param {Number} id weapon id
   * @param {String} name weapon name
   * @param {String} description weapon description
   * @param {Number} damage weapon damage
   * @param {Number} accuracy weapon accuracy
   * @param {Number} firerate weapon firerate
   * @param {Boolean} price weapon price
   * @return {weapons: Weapons} the new weapon
   * @memberof NightCity
   */
  async updateWeapon(id, name, description, damage, accuracy, firerate, price) {
    try {
      return this.db.addData(Weapons, new Weapons(id, name, description, damage, accuracy, firerate, price));
    }
    catch (err) {
      throw err;
    }
  }


  /*********************************************************
   * JOB
   ********************************************************/

  /**
   * Add a job to the database
   * @param {String} fixer the name of the fixer
   * @param {String} title the title of the job
   * @param {String} description the description of the job
   * @param {Number} henchmenCount the number of foes
   * @param {Number} reward the reward value
   * @return {Jobs: Jobs} the new job
   * @memberof NightCity
   */
  async createJob(fixer, title, description, henchmenCount, reward) {
    try {
      if (!fixer || fixer.trim() === "") {
        throw new exc.NoFixerException();
      }
      if (!title || title.trim() === "") {
        throw new exc.NoTitleException();
      }
      if (!description) {
        throw new exc.NoDescriptionException();
      }
      if ((henchmenCount !== 0 && !henchmenCount) || henchmenCount < 0) {
        throw new exc.NoHenchmenException();
      }
      if ((reward !== 0 && !reward) || reward <= 0) {
        throw new exc.NoRewardException();
      }

      return this.db.addData(Jobs, new Jobs(null, fixer, title, description, henchmenCount, reward, true));
    }
    catch (err) {
      throw err;
    }
  }

  /**
   * Find a list of jobs using their titles
   * @param {String} title job title
   */
  async getJobsByTitle(title) {
    try {
      if (title) {
        const jobs = await this.db.getData(Jobs, { where: { title: Like(`%${title}%`) }, order: {isAvailable: "DESC", id:"DESC"} });
        if (jobs.length == 0)
          throw new exc.NoJobException();
        return jobs;
      }
      return await this.db.getData(Jobs, {order: {isAvailable: "DESC", id:"DESC"}});
    }
    catch (err) {
      throw err;
    }
  }

  
  /**
   * Update a job to the database
   * @param {Number} id merc id
   * @param {String} fixer the new fixer name
   * @param {String} title the new job title
   * @param {String} description the new job description
   * @param {Number} henchmenCount the new henchmen number
   * @param {Number} reward the new reward amount
   * @param {Boolean} isAvailable the new job status
   * @return {Jobs: Jobs} the new job
   * @memberof NightCity
   */
  async updateJob(id, fixer, title, description, henchmenCount, reward, isAvailable) {
    try {
      return this.db.addData(Jobs, new Jobs(id, fixer, title, description, henchmenCount, reward, isAvailable));
    }
    catch (err) {
      throw err;
    }
  }

  /*********************************************************
   * MERC
   ********************************************************/

  /**
   * Add a merc to the database
   * @param {String} nickname merc name
   * @param {Number} legalAge merc age
   * @param {Number} eddies merc eddies
   * @return {merc: Merc} the new merc
   * @memberof NightCity
   */
  async createMerc(nickname, legalAge, eddies) {
    try {
      if (!nickname || nickname.trim() === "") {
        throw new exc.NoNicknameException();
      }
      if ((legalAge !== 0 && !legalAge) || legalAge < 0) {
        throw new exc.NoLegalAgeException();
      }
      if ((eddies !== 0 && !eddies) || eddies < 0) {
        throw new exc.NoEddiesException();
      }
      return this.db.addData(Mercs, new Mercs(null, nickname, legalAge, 1, eddies, true));
    }
    catch (err) {
      throw err;
    }
  }

  /**
   * Find a list of mercs using their names
   * @param {String} name merc name
   */
  async getMercsByName(name) {
    try {
      let mercs = []
      if (name) {
        mercs = await this.db.getData(Mercs, { where: { nickname: Like(`%${name}%`) }, order: {isAlive: "DESC", id:"DESC"}});
        if (mercs.length === 0)
          throw new exc.NoMercException();
      }
      else {
        mercs = await this.db.getData(Mercs, {order: {isAlive: "DESC", id:"DESC"}});
      }

      for(let i = 0; i < mercs.length ; i++){
        const weapons = await this.db.getData(Weapons, { where: {id: mercs[i].idWeapon}})
        mercs[i].weaponName = weapons[0].name
      }
      
      return mercs
    }
    catch (err) {
      throw err;
    }
  }

  
  /**
   * Update a merc to the database
   * @param {Number} id merc id
   * @param {String} nickname the new merc nickname
   * @param {Number} legalAge the new merc legalAge
   * @param {Number} idWeapon the new merc weapon id
   * @param {Number} eddies the new merc eddies
   * @param {Boolean} isAlive the new merc status
   * @return {Mercs: Mercs} the new merc
   * @memberof NightCity
   */
  async updateMerc(id, nickname, legalAge, idWeapon, eddies, isAlive) {
    try {
      return this.db.addData(Mercs, new Mercs(id, nickname, legalAge, idWeapon, eddies, isAlive));
    }
    catch (err) {
      throw err;
    }
  }

  /*********************************************************
   * PROCESS
   ********************************************************/

  /**
   * Assign a new weapon to a merc and debit weapon value from the merc eddies
   * @param {Number} merc_id merc ID
   * @param {Number} weapon_id weapon ID
   * @return {merc: Merc} the updated merc
   * @memberof NightCity
   */
  async buyWeapon(merc_id, weapon_id) {
    if (!merc_id || !weapon_id) {
      throw new exc.NoIdException();
    }
    try {
      const mercs = await this.db.getData(Mercs, { where: { id: merc_id } });
      const weapons = await this.db.getData(Weapons, { where: { id: weapon_id } });

      if (!mercs[0]) {
        throw new exc.UnexistingMercException();
      }
      if (!weapons[0]) {
        throw new exc.UnexistingWeaponException();
      }
      if (mercs[0].eddies < weapons[0].price) {
        throw new exc.NotEnoughEddiesException();
      }
      return await this.db.addData(Mercs, new Mercs(mercs[0].id, mercs[0].nickname, mercs[0].legalAge, weapon_id, mercs[0].eddies - weapons[0].price, mercs[0].isAlive));
    }
    catch (err) {
      throw err;
    }
  }

  /**
   * Launch the mission and update engaged merc
   * @param {Number} merc_id merc ID
   * @param {Number} job_id job ID
   * @return {String[]} the mission report log
   * @memberof NightCity
   */
  async executeMission(job_id, merc_id) {
    const CONFIG = {
      pnjLife: 100,
      pnjDamage: 10,
      mercLife: 100
    }

    if (!merc_id || !job_id) {
      throw new exc.NoIdException();
    }

    let mercs,jobs,weapons
    try {
      mercs = await this.db.getData(Mercs, { where: { id: merc_id } });
      jobs = await this.db.getData(Jobs, { where: { id: job_id } });

      if (!mercs[0]) {
        throw new exc.UnexistingMercException();
      }
      if (!jobs[0]) {
        throw new exc.UnexistingJobException();
      }
      if (!mercs[0].isAlive) {
        throw new exc.DeadMercException();
      }
      if (!jobs[0].isAvailable) {
        throw new exc.CompletedJobException();
      }
      weapons = await this.db.getData(Weapons, { where: { id: mercs[0].idWeapon } });
      if (!weapons[0]) {
        throw new exc.UnexistingWeaponException();
      }
    }
    catch (err) {
      throw err;
    }

    const merc = mercs[0];
    const job = jobs[0];
    const weapon = weapons[0];
    const report = new Report(job, merc, weapon)

    let mercLife = CONFIG.mercLife;
    let remainingHenchmen = job.henchmenCount;
    let henchmanLife
    // tant qu'il y a des ennemies et que le merc est vivant
    while (remainingHenchmen > 0 && mercLife > 0) {

      // on crée une cible sur patte full life
      henchmanLife = CONFIG.pnjLife
      let henchmanId = job.henchmenCount - remainingHenchmen + 1
      report.addData(new ReportLine(mercLife, henchmanLife, `Arrival of the enemy #${henchmanId}`))

      // tant que la cible sur patte et le merc sont vivants
      while (henchmanLife > 0 && mercLife > 0) {

        // calcul des dégats du merc
        for (let nbShot = 0; nbShot < weapon.firerate; nbShot++) {
          const aleaToHit = Math.floor((Math.random() * 100) + 1)

          // si le tir touche
          if (aleaToHit <= weapon.accuracy) {
            henchmanLife -= weapon.damage;

            // si la cible sur patte survis 
            if (henchmanLife > 0) {
              report.addData(new ReportLine(mercLife, henchmanLife, `${merc.nickname} shoots successfully and inflicts ${weapon.damage} damages`))
            }
            // si la cible sur patte décède
            else {
              report.addData(new ReportLine(mercLife, henchmanLife, `${merc.nickname} shoots successfully and inflicts ${weapon.damage} damages killing enemy #${henchmanId}`))
              remainingHenchmen -= 1
              break
            }
          }
          // si le tir échoue
          else {
            report.addData(new ReportLine(mercLife, henchmanLife, `${merc.nickname} misses his shot on enemy #${henchmanId}`))
          }
        }

        // si la cible sur patte est enncore en vie 
        if (henchmanLife > 0) {
          mercLife -= CONFIG.pnjDamage
          report.addData(new ReportLine(mercLife, henchmanLife, `Enemy #${henchmanId} attacks and inflicts ${CONFIG.pnjDamage} damages to ${merc.nickname}`))
        }

      }

    }

    // s'il n'y a plus de cible sur patte le merc a gagné
    if (remainingHenchmen === 0) {
      report.addData(new ReportLine(mercLife, henchmanLife, `${merc.nickname} passed '${job.title}' and wins ${job.reward} Eurodolars`))
      merc.eddies += job.reward;
      job.isAvailable = false;
    }
    // le merc est mort et le job est un échec
    else {
      report.addData(new ReportLine(mercLife, henchmanLife, `${merc.nickname} did not survive, '${job.title}' is a failure`))
      merc.isAlive = false
      job.henchmenCount = remainingHenchmen;
    }

    // Mise à jour de la base de données
    try {
      await this.db.addData(Jobs, job);
      await this.db.addData(Mercs, merc);
    }
    catch (err) {
      throw err;
    }

    report.merc = merc
    report.job = job

    return report
  }

}

const nightCity = new NightCity();

export const getNightCity = () => nightCity;
