export default class Report {

    constructor(job, merc, weapon){
        this.job = job;
        this.merc = merc;
        this.weapon = weapon;
        this.data = []
    }

    addData(reportLine){
        this.data = [...this.data, reportLine]
    }
}