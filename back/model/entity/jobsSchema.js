import Jobs from './jobs'
import { EntitySchema } from 'typeorm'


/*
    id INT PRIMARY KEY AUTO_INCREMENT,
	fixer NVARCHAR(50) NOT NULL,
	title NVARCHAR(50) NOT NULL,
	description NVARCHAR(500) NOT NULL,
	henchmenCount INT NOT NULL,
	reward INT NOT NULL,
	isAvailable TINYINT NOT NULL DEFAULT 1
*/

export const jobsSchema = new EntitySchema({
    tableName: 'Jobs',
    name: 'Jobs',
    target: Jobs,
    columns: {
        id: {
            primary: true,
            generated: true,
            type: 'int'
        },
        fixer: {
            type: 'varchar',
            nullable: false
        },
        title: {
            type: 'varchar',
            nullable: false
        },
        description: {
            type: 'varchar',
            nullable: false
        },
        henchmenCount: {
            type: 'int',
            nullable: false
        },
        reward: {
            type: 'int',
            nullable: false
        },
        isAvailable: {
            type: 'boolean',
            nullable: false
        }
    }
})