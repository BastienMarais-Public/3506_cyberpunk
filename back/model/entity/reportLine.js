export default class ReportLine {

    constructor(merc_life,henchman_life, description){
        this.merc_life = merc_life;
        this.henchman_life = henchman_life;
        this.description = description;
    }
}