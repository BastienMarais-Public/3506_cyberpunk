class Mercs {

    /**
     * Creates an instance of Mercs.
     * @param {number} id
     * @param {string} nickname
     * @param {number} legalAge
     * @param {number} idWeapon
     * @param {number} eddies
     * @param {boolean} isAlive
     * @memberof Mercs
     */

    constructor(id, nickname, legalAge, idWeapon, eddies, isAlive) {
        this.id = id
        this.nickname = nickname
        this.legalAge = legalAge
        this.idWeapon = idWeapon
        this.eddies = eddies
        this.isAlive = isAlive
    }
}

export default Mercs