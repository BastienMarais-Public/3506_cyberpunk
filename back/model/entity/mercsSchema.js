import Mercs from './mercs'
import { EntitySchema } from 'typeorm'


/*
    id INT PRIMARY KEY AUTO_INCREMENT,
	nickname NVARCHAR(50) NOT NULL,
	legalAge INT NOT NULL,
	idWeapon INT NULL DEFAULT 1,
	eddies INT NOT NULL DEFAULT 0,
	isAlive TINYINT NOT NULL DEFAULT 1
*/

export const mercsSchema = new EntitySchema({
    tableName: 'Mercs',
    name: 'Mercs',
    target: Mercs,
    columns: {
        id: {
            primary: true,
            generated: true,
            type: 'int'
        },
        nickname: {
            type: 'varchar',
            nullable: false
        },
        legalAge: {
            type: 'int',
            nullable: false
        },
        idWeapon: {
            type: 'int',
            nullable: false
        },
        eddies: {
            type: 'int',
            nullable: false
        },
        isAlive: {
            type: 'boolean',
            nullable: false
        }
    }
})