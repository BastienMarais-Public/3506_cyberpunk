class Weapons {

    /**
     * Creates an instance of Weapons.
     * @param {number} id
     * @param {string} name
     * @param {string} description
     * @param {number} damage
     * @param {number} accuracy
     * @param {number} firerate
     * @param {number} price
     * @memberof Weapons
     */

    constructor(id, name, description, damage, accuracy, firerate, price) {
        this.id = id
        this.name = name
        this.description = description
        this.damage = damage
        this.accuracy = accuracy
        this.firerate = firerate
        this.price = price
    }
}

export default Weapons