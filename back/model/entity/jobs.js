class Jobs {

    /**
     * Creates an instance of Jobs.
     * @param {number} id
     * @param {string} fixer
     * @param {string} title
     * @param {string} description
     * @param {number} henchmenCount
     * @param {number} reward
     * @param {boolean} isAvailable
     * @memberof Jobs
     */

    constructor(id, fixer , title, description, henchmenCount, reward, isAvailable) {
        this.id = id
        this.fixer = fixer
        this.title = title
        this.description = description
        this.henchmenCount = henchmenCount
        this.reward = reward
        this.isAvailable = isAvailable
    }
}

export default Jobs