import Weapons from './weapons'
import { EntitySchema } from 'typeorm'


/*
    id INT PRIMARY KEY AUTO_INCREMENT,
	name NVARCHAR(50) NOT NULL,
	description NVARCHAR(500) NOT NULL,
	damage INT NOT NULL,
	accuracy INT NOT NULL,
	firerate INT NOT NULL,
	price INT NOT NULL
*/

export const weaponsSchema = new EntitySchema({
    tableName: 'Weapons',
    name: 'Weapons',
    target: Weapons,
    columns: {
        id: {
            primary: true,
            generated: true,
            type: 'int'
        },
        name: {
            type: 'varchar',
            nullable: false
        },
        description: {
            type: 'varchar',
            nullable: false
        },
        damage: {
            type: 'int',
            nullable: false
        },
        accuracy: {
            type: 'int',
            nullable: false
        },
        firerate: {
            type: 'int',
            nullable: false
        },
        price: {
            type: 'int',
            nullable: false
        }
    }
})