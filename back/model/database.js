import { createConnection} from 'typeorm'
import { jobsSchema } from './entity/jobsSchema'
import { mercsSchema } from './entity/mercsSchema'
import { weaponsSchema } from './entity/weaponsSchema'


class Database {

  async connect() {
    try {
      return await createConnection({
        name: 'connexion-' + Date.now() + "-" + Math.floor(Math.random() * Math.floor(1000)),
        type: 'mysql',
        host: '0.0.0.0',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'db_cyberpunk2077',
        entities: [jobsSchema, mercsSchema, weaponsSchema]
      })
    } 
    catch (err) {
      throw err
    }
  }

  // where ==> { where: { firstName: "Timber", lastName: "Saw" } }
  // https://github.com/typeorm/typeorm/blob/master/docs/find-options.md
  async getData(repo, where) {
    const connection = await this.connect()
    try {
      const dataRepository = connection.getRepository(repo)

      return await dataRepository.find(where)
    } catch (err) {
      console.error(err.message)
      throw err
    } finally {
      await connection.close()
    }
  }

  async addData(repo, object) {
    const connection = await this.connect()

    try {
      const dataRepository = connection.getRepository(repo)

      return await dataRepository.save(object)
    } catch (err) {
      console.error(err.message)
      throw err
    } finally {
      await connection.close()
    }
  }

  async deleteData(repo, object) {
    const connection = await this.connect()

    try {
      const dataRepository = connection.getRepository(repo)

      return await dataRepository.delete(object)
    } catch (err) {
      console.error(err.message)
      throw err
    } finally {
      await connection.close()
    }
  }
}

export default Database

