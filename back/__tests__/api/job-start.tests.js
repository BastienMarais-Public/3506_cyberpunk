import request from 'supertest'
import app from '../../api/app'
import { UnexistingMercException, UnexistingJobException, DeadMercException, CompletedJobException, UnexistingWeaponException } from '../../business/error';
import { getNightCity } from '../../business/nightCity'

const merc_id = 1;
const job_id = 4;
const weapon_id = 4;
const v = {
    "id": merc_id,
    "nickname": "V",
    "legalAge": 27,
    "idWeapon": 4,
    "eddies": 2000,
    "isAlive": true,
}
const burnNightCity = {
    "id": job_id,
    "fixer": "Rogue",
    "title": "Burn Night City",
    "description": "Time to give Arasaka what it deserves!\nWe will gather a team to invade their siege and plant a nuclear device inside, on Aug. 20, 2023.\nJob is risky, yeah, I won't lie. But it will be worth it. Expect a fair compensation if you manage to survive.\nMorgan Blackhand, Johnny Silverhand, Spider Murphy and myself are going.\nJoin us!",
    "henchmenCount": 10000,
    "reward": 1,
    "isAvailable": false,
}
const nekomata = {
    "id": weapon_id,
    "name": "Nekomata",
    "description": "Semi automatic chargeable steel ammunition sniper rifle. Pierces concrete like cardboard.",
    "damage": 100,
    "accuracy": 95,
    "firerate": 1,
    "price": 700
}

beforeEach(() => {
    jest.clearAllMocks();
});

describe('Launching the mission', () => {
    it('The mission is a success/failure', async (done) => {
        const expectedResponseBody = {
            "job": { burnNightCity },
            "merc": { v },
            "weapon": { nekomata },
            "data": [],
        };

        const executeMission = jest.fn().mockReturnValue(expectedResponseBody);
        getNightCity().executeMission = executeMission;

        request(app)
            .post(`/job/start/${job_id}/${merc_id}`)
            .expect(200)
            .expect(response => {
                expect(response.body).toEqual(expectedResponseBody);
                expect(executeMission).toHaveBeenCalledTimes(1);
            })
            .end(done);
    });

    it('The selected merc doesn\'t exists', async (done) => {
        const executeMission = jest.fn().mockImplementation( () => { throw new UnexistingMercException});
        getNightCity().executeMission = executeMission;

        request(app)
            .post(`/job/start/${job_id}/${merc_id}`)
            .expect(400)
            .expect(response => {
                expect(response.body).toEqual("No merc with such ID");
                expect(executeMission).toHaveBeenCalledTimes(1);
                expect(executeMission).toThrow(UnexistingMercException);

            })
            .end(done);
    });

    it('The selected job doesn\'t exists', async (done) => {
        const executeMission = jest.fn().mockImplementation( () => { throw new UnexistingJobException});
        getNightCity().executeMission = executeMission;

        request(app)
            .post(`/job/start/${job_id}/${merc_id}`)
            .expect(400)
            .expect(response => {
                expect(response.body).toEqual("No job with such ID");
                expect(executeMission).toHaveBeenCalledTimes(1);
                expect(executeMission).toThrow(UnexistingJobException);

            })
            .end(done);
    });

    it('The selected merc is dead', async (done) => {
        const executeMission = jest.fn().mockImplementation( () => { throw new DeadMercException});
        getNightCity().executeMission = executeMission;

        request(app)
            .post(`/job/start/${job_id}/${merc_id}`)
            .expect(400)
            .expect(response => {
                expect(response.body).toEqual("This merc is already dead, remember ?");
                expect(executeMission).toHaveBeenCalledTimes(1);
                expect(executeMission).toThrow(DeadMercException);

            })
            .end(done);
    });

    it('The selected job has already been completed', async (done) => {
        const executeMission = jest.fn().mockImplementation( () => { throw new CompletedJobException});
        getNightCity().executeMission = executeMission;

        request(app)
            .post(`/job/start/${job_id}/${merc_id}`)
            .expect(400)
            .expect(response => {
                expect(response.body).toEqual("This mission has already been completed");
                expect(executeMission).toHaveBeenCalledTimes(1);
                expect(executeMission).toThrow(CompletedJobException);

            })
            .end(done);
    });

    it('The selected weapon doesn\'t exists', async (done) => {
        const executeMission = jest.fn().mockImplementation( () => { throw new UnexistingWeaponException});
        getNightCity().executeMission = executeMission;

        request(app)
            .post(`/job/start/${job_id}/${merc_id}`)
            .expect(400)
            .expect(response => {
                expect(response.body).toEqual("No weapon with such ID");
                expect(executeMission).toHaveBeenCalledTimes(1);
                expect(executeMission).toThrow(UnexistingWeaponException);

            })
            .end(done);
    });
})