const { async } = require("regenerator-runtime");
const { getNightCity } = require("../../business/nightCity")
const { 
    NoIdException,
    UnexistingMercException,
    UnexistingJobException,
    UnexistingWeaponException,
    DeadMercException,
    CompletedJobException,
} = require("../../business/error");

const merc_id = 1;
const dead_merc_id = 2;
const job_id = 4;
const completed_job_id = 5;
const weapon_id = 4;

const mockAddData = jest.fn();
const mockGetData = jest.fn();
const mockDeleteData = jest.fn();
let v = jest.fn();
let dead_v = jest.fn();
let burnNightCity = jest.fn();
let completed_burnNightCity = jest.fn();
let nekomata = jest.fn();

beforeEach(() => {
    v = {
        "id": merc_id,
        "nickname": "V",
        "legalAge": 27,
        "idWeapon": 4,
        "eddies": 2000,
        "isAlive": true,
    };
    
    dead_v = {
        "id": dead_merc_id,
        "nickname": "dead V",
        "legalAge": 27,
        "idWeapon": 4,
        "eddies": 0,
        "isAlive": false,
    };
    
    burnNightCity = {
        "id": job_id,
        "fixer": "Rogue",
        "title": "Burn Night City",
        "description": "Time to give Arasaka what it deserves!\nWe will gather a team to invade their siege and plant a nuclear device inside, on Aug. 20, 2023.\nJob is risky, yeah, I won't lie. But it will be worth it. Expect a fair compensation if you manage to survive.\nMorgan Blackhand, Johnny Silverhand, Spider Murphy and myself are going.\nJoin us!",
        "henchmenCount": 100,
        "reward": 1,
        "isAvailable": true,
    };
    
    completed_burnNightCity = {
        "id": completed_job_id,
        "fixer": "Rogue",
        "title": "Burn Night City",
        "description": "Time to give Arasaka what it deserves!\nWe will gather a team to invade their siege and plant a nuclear device inside, on Aug. 20, 2023.\nJob is risky, yeah, I won't lie. But it will be worth it. Expect a fair compensation if you manage to survive.\nMorgan Blackhand, Johnny Silverhand, Spider Murphy and myself are going.\nJoin us!",
        "henchmenCount": 0,
        "reward": 0,
        "isAvailable": false,
    };
    
    nekomata = {
        "id": weapon_id,
        "name": "Nekomata",
        "description": "Semi automatic chargeable steel ammunition sniper rifle. Pierces concrete like cardboard.",
        "damage": 100,
        "accuracy": 95,
        "firerate": 1,
        "price": 700
    };

    const db = {
        addData: mockAddData,
        getData: mockGetData,
        deleteData: mockDeleteData,
    };

    getNightCity().db = db;
    jest.clearAllMocks();
})

describe('night city tests', () => {
    describe('executeMission()', () => {
        it('Mission completed/failed', async () => { 

            mockGetData.mockReturnValueOnce([v])
                .mockReturnValueOnce([burnNightCity])
                .mockReturnValueOnce([nekomata]);

            const returnValue = await getNightCity().executeMission(job_id, merc_id);
            expect(returnValue.merc.name).toStrictEqual(v.name);
            expect(returnValue.merc.legalAge).toStrictEqual(v.legalAge);
            expect(returnValue.merc.id_weapon).toStrictEqual(v.id_weapon);
            expect(returnValue.job.fixer).toStrictEqual(burnNightCity.fixer);
            expect(returnValue.job.title).toStrictEqual(burnNightCity.title);
            expect(returnValue.job.description).toStrictEqual(burnNightCity.description);
            expect(returnValue.job.reward).toStrictEqual(burnNightCity.reward);
            expect(returnValue.weapon).toStrictEqual(nekomata);
            expect(mockGetData).toHaveBeenCalledTimes(3);
            expect(mockAddData).toHaveBeenCalledTimes(2);
        });

        it('No ids', async () => {
            try {
                await getNightCity().executeMission();
            }
            catch(err){
                expect(err).toStrictEqual(new NoIdException());
            }
            expect(mockGetData).toHaveBeenCalledTimes(0);
            expect(mockAddData).toHaveBeenCalledTimes(0);
        });

        it('Merc doesn\'t exists', async () => {
            mockGetData.mockReturnValueOnce([])
                .mockReturnValueOnce([burnNightCity]);

            try {
                await getNightCity().executeMission(job_id, merc_id);
            }
            catch(err){
                expect(err).toStrictEqual(new UnexistingMercException());
            }
            expect(mockGetData).toHaveBeenCalledTimes(2);
            expect(mockAddData).toHaveBeenCalledTimes(0);
        });
        
        it('Job doesn\'t exists', async () => {
            mockGetData.mockReturnValueOnce([v])
                .mockReturnValueOnce([]);

            try {
                await getNightCity().executeMission(job_id, merc_id);
            }
            catch(err){
                expect(err).toStrictEqual(new UnexistingJobException());
            }
            expect(mockGetData).toHaveBeenCalledTimes(2);
            expect(mockAddData).toHaveBeenCalledTimes(0);
        });

        it('Merc already dead', async () => {
            mockGetData.mockReturnValueOnce([dead_v])
                .mockReturnValueOnce([burnNightCity]);

            try {
                await getNightCity().executeMission(job_id, merc_id);
            }
            catch(err){
                expect(err).toStrictEqual(new DeadMercException());
            }
            expect(mockGetData).toHaveBeenCalledTimes(2);
            expect(mockAddData).toHaveBeenCalledTimes(0);
        });

        it('Job already completed', async () => {
            mockGetData.mockReturnValueOnce([v])
                .mockReturnValueOnce([completed_burnNightCity]);

            try {
                await getNightCity().executeMission(job_id, merc_id);
            }
            catch(err){
                expect(err).toStrictEqual(new CompletedJobException());
            }
            expect(mockGetData).toHaveBeenCalledTimes(2);
            expect(mockAddData).toHaveBeenCalledTimes(0);
        });

        it('Weapon doesn\'t exists', async () => {
            mockGetData.mockReturnValueOnce([v])
                .mockReturnValueOnce([burnNightCity])
                .mockReturnValueOnce([]);

            try {
                await getNightCity().executeMission(job_id, merc_id);
            }
            catch(err){
                expect(err).toStrictEqual(new UnexistingWeaponException());
            }
            expect(mockGetData).toHaveBeenCalledTimes(3);
            expect(mockAddData).toHaveBeenCalledTimes(0);
        });
    });
});