# Projet final - Cyberpunk 2077 :

* Yoan Belhous
* Erwan Desaint
* Bastien Marais

## Installation :

```sh
npm i -g pnpm

cd back
pnpm i
pnpm test
pnpm start

cd front
pnpm i
pnpm test
npm start
```

## Caractéristiques du projet :

### Back

* Un postman est disponible pour tester l'API, une variable ROOT_API est paramétrable si besoin pour changer l'adresse de l'api.
* Des fonctions `update` sont présentes uniquement pour tester l'API et/ou surtout pour refaire vivre des mercs et rendre dispo les jobs.
* Nous avons décidé lorsqu'un job échoue, de retirer les sous fifres tués par le merc décédé afin de donner de l'importance à chaque mission et permettre la stratégie bourrine d'envoyer 25 mercs sur un même job.
* Les messages d'erreurs de l'API sont également précis afin de faciliter la compréhension de l'utilisateur.
* Pour faciliter la récupération des informations nous avons choisi de remonter en premier les éléments vivant/dispo et dernièrement créés.
* Vous pouvez directement taper dessus depuis : https://cyber-api.bastienmarais.fr

### Front

* Le thème du site s'inspire du menu du jeu, donc on retrouve du rouge, du bleu et du jaune principalement.
* Une excellente musique de fond est disponible choisi au sein de l'univers du jeu
* Nous avons choisi de faire de ce site une page composée de plusieurs éléments fixes responsives.
    - Un bouton gérant la musique
    - Un bouton bugé menant à la page 404 sans reload (faudrait pas interrompre la musique quand même !)
    - Un logo du jeu
    - La partie gauche affichant le menu
    - La partie droite affichant le contenu du site
* Un rapport de combat est affiché pour avoir le détail du job
* Pour le faire taper sur une autre API il suffit de modifier une seule variable présente dans `src/api/callApi.js`
* Vous pouvez directement jouer sur la version en prod : https://cyber-site.bastienmarais.fr


